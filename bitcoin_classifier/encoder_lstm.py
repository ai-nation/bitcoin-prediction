from __future__ import print_function
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

class Encoder_LSTM(nn.Module):
    def __init__(self):
        super(Encoder_LSTM, self).__init__()
        self.lstm1 = nn.LSTMCell(1, 51)
        self.lstm2 = nn.LSTMCell(51, 51)

    def forward(self, input):
        outputs = []
        h_t = Variable(torch.randn(input.size(0), 51).double(), requires_grad=False)
        c_t = Variable(torch.randn(input.size(0), 51).double(), requires_grad=False)
        h_t2 = Variable(torch.randn(input.size(0), 51).double(), requires_grad=False)
        c_t2 = Variable(torch.randn(input.size(0), 51).double(), requires_grad=False)

        for i, input_t in enumerate(input.chunk(input.size(1), dim=1)):
            h_t, c_t = self.lstm1(input_t, (h_t, c_t))
            h_t2, c_t2 = self.lstm2(h_t, (h_t2, c_t2))
        return c_t, c_t2
