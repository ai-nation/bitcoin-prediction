from __future__ import print_function
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

class Decoder_LSTM(nn.Module):
    def __init__(self):
        super(Decoder_LSTM, self).__init__()
        self.lstm1 = nn.LSTMCell(1, 51)
        self.lstm2 = nn.LSTMCell(51, 51)
        self.linear = nn.Linear(51, 1)

    def forward(self, input, c_t_enc,c_t2_enc, future):
        outputs = []
        h_t = Variable(torch.randn(input.size(0), 51).double(), requires_grad=False)
        h_t2 = Variable(torch.randn(input.size(0), 51).double(), requires_grad=False)
        c_t = c_t_enc
        c_t2 = c_t2_enc
        h_t, c_t = self.lstm1(input, (h_t, c_t))
        h_t2, c_t2 = self.lstm2(h_t, (h_t2, c_t2))
        output = self.linear(h_t2)
        outputs += [output]
        for i in range(future-1):# if we should predict the future
            h_t, c_t = self.lstm1(output, (h_t, c_t))
            h_t2, c_t2 = self.lstm2(h_t, (h_t2, c_t2))
            output = self.linear(h_t2)
            outputs += [output]
        outputs = torch.stack(outputs, 1).squeeze(2)
        return outputs

