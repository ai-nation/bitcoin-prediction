from __future__ import print_function
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from bitcoin_classifier import Sequence
import sys

class Params():
    LEARN_RATE = 0.01
    GRADIENT_CUTOFF = 0.01


if __name__ == '__main__':
    # set parameters
    if len(sys.argv) == 3:
        Params.LEARN_RATE = float(sys.argv[1])
        Params.GRADIENT_CUTOFF = float(sys.argv[2])

    # set random seed to 0
    np.random.seed(0)
    torch.manual_seed(0)
    # load data and make training set
    data = torch.load('traindata.pt')
    #load training data
    input = Variable(torch.from_numpy(data[3:, :-1]), requires_grad=False)
    #target = label
    target = Variable(torch.from_numpy(data[3:, 1:]), requires_grad=False)
    #test data
    test_input = Variable(torch.from_numpy(data[:3, :-1]), requires_grad=False)
    test_target = Variable(torch.from_numpy(data[:3, 1:]), requires_grad=False)
    # build the model
    seq = Sequence()
    seq.double()
    criterion = nn.MSELoss() #loss function
    # use LBFGS as optimizer since we can load the whole data to train
    optimizer = optim.LBFGS(seq.parameters(), lr=Params.LEARN_RATE)
    #begin to train
    train_past_init = 40
    train_future_init = 5
    train_past = train_past_init
    train_future = train_future_init
    for i in range(input.size(1)):
        train_past = int(train_past_init + 5*(i-i%50)/50)
        train_future = int(train_future_init + (i-i%20)/20)
        if i+train_future >= input.size(1):
            break
        print('STEP: ', i)
        train_input = input[:, i:i+train_past]
        train_target = input[:, i+train_past:i+train_past+train_future]
        def closure():
            optimizer.zero_grad()
            out = seq(train_input, train_future)
            loss = criterion(out, train_target)
            print('loss:', loss.data.numpy()[0])
            loss.backward()
            torch.nn.utils.clip_grad_norm(seq.parameters(), Params.GRADIENT_CUTOFF)
            return loss
        if i%25 == 0:
            out = seq(train_input, train_future)
            yp = train_input.data.numpy()[0]
            yf = out.data.numpy()[0]
            y = np.append(yp, yf)
            plt.figure(figsize=(30,10))
            plt.title('Predict future values for time sequences\n(Dashlines are predicted values)', fontsize=30)
            plt.xlabel('x', fontsize=20)
            plt.ylabel('y', fontsize=20)
            plt.xticks(fontsize=20)
            plt.yticks(fontsize=20)
            def draw(yi, color):
                plt.plot(np.arange(train_past), yi[:train_past], color, linewidth = 2.0)
                plt.plot(np.arange(train_past, train_past + train_future), yi[train_past:], color + ':', linewidth = 2.0)
            draw(y, 'r')
            plt.savefig('plots/predict%d.pdf' % i)
            plt.close()

        optimizer.step(closure)

    #begin to predict
    future = 1000
    pred = seq(test_input, future = future)
    loss = criterion(pred[:, :-future], test_target)
    print('test loss:', loss.data.numpy()[0])
    y = pred.data.numpy()
    print('train_past', train_past)
    print('train_future', train_future)
    #draw the result
    plt.figure(figsize=(30,10))
    plt.title('Predict future values for time sequences\n(Dashlines are predicted values)', fontsize=30)
    plt.xlabel('x', fontsize=20)
    plt.ylabel('y', fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    def draw(yi, color):
        plt.plot(np.arange(input.size(1)), yi[:input.size(1)], color, linewidth = 2.0)
        plt.plot(np.arange(input.size(1), input.size(1) + future), yi[input.size(1):], color + ':', linewidth = 2.0)
    draw(y[0], 'r')
    draw(y[1], 'g')
    draw(y[2], 'b')
    plt.savefig('plots/final%d.pdf' % i)
    plt.close()
