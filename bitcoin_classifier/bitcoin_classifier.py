from __future__ import print_function
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
import numpy as np
import matplotlib
from encoder_lstm import Encoder_LSTM
from decoder_lstm import Decoder_LSTM
matplotlib.use('Agg')
import matplotlib.pyplot as plt

class Sequence(nn.Module):
    def __init__(self):
        super(Sequence, self).__init__()
        self.encoderLstm = Encoder_LSTM()
        self.decoderLstm = Decoder_LSTM()

    def forward(self, input, future = 0):
        c_t, c_t2 = self.encoderLstm(input)
        outputs = self.decoderLstm(input[:, -1:], c_t, c_t2, future)
        return outputs

