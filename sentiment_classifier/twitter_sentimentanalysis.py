#!/usr/bin/env python3

import os

import re
from dotenv import load_dotenv, find_dotenv

import json
import tweepy
from os.path import join

load_dotenv(find_dotenv())

auth = tweepy.OAuthHandler(os.environ.get("CONSUMER_KEY"), os.environ.get("CONSUMER_SECRET"))
auth.set_access_token(os.environ.get("ACCESS_TOKEN"), os.environ.get("ACCESS_TOKEN_SECRET"))

api = tweepy.API(auth)


class MyStreamListener(tweepy.StreamListener):
    def __init__(self):
        self.save_file = open(join(os.environ.get("HOME"), 'Desktop', 'tweets.json'), 'w+')
        self.tweets = []
        super(MyStreamListener, self).__init__()

    def on_data(self, data):
        tweet = json.loads(data)['text']
        tweet = self.transform(tweet)
        self.tweets.append(tweet)
        print(len(self.tweets))
        if len(self.tweets) > 100:
            self.save_file.write(json.dumps(self.tweets))
            self.save_file.close()
            return False
        return True

    def transform(self, tweet):
        hashtags = re.findall(r"#(\w+)", tweet)
        tweet = tweet.lower()
        tweet = tweet.replace('rt', '')
        tweet = tweet.replace('#', '')
        tweet = re.sub(r'@.*?:', '', tweet)
        tweet = re.sub(r'\s+', ' ', tweet)
        tweet = tweet.strip()
        return {'tweet': tweet, 'hashtags': hashtags}


print("initializing stream...")
myStream = tweepy.Stream(auth=api.auth, listener=MyStreamListener())
myStream.filter(track=['crypto'])
