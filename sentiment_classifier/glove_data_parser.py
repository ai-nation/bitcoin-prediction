from os.path import join
import os
import re


def list_of_vectors(string_vectors):
    print ([float(vector) for vector in string_vectors.split()])
    return []


def tokenize(sentence):
    sentence = sentence.lower()
    sentence = sentence.replace('\n', '')
    sentence = re.sub('https?://\S+\b|www\.(\w+\.)+\S*', '<URL>', sentence)
    sentence = re.sub('/', ' / ', sentence)
    sentence = re.sub('@\w+/', '<USER>', sentence)
    sentence = re.sub('#{eyes}#{nose}[)d]+|[)d]+#{nose}#{eyes}', '<SMILE>', sentence)
    sentence = re.sub('#{eyes}#{nose}p+', '<LOLFACE>', sentence)
    sentence = re.sub('#{eyes}#{nose}\(+|\)+#{nose}#{eyes}', '<SADFACE>', sentence)
    sentence = re.sub('#{eyes}#{nose}[/|l*]', '<NEUTRALFACE>', sentence)
    sentence = re.sub('<3', '<HEART>', sentence)
    sentence = re.sub('[-+]?[.\d]*[\d]+[:,.\d]*', '<NUMBER>', sentence)
    words = re.split("\s", sentence)
    return [word for word in words if word != '']


def generate_vectors():
    vector_list = []
    sample_words = []
    word2vec = {}

    with open(join(os.environ.get("HOME"), 'Desktop', 'glove.twitter.27B.50d.txt'), 'r+') as pretrained_vectors:
        for line in pretrained_vectors.readlines():
            label = re.match('^\S+', line)
            if label:
                label = label[0]
                vector = [float(v) for v in line.replace(f'{label} ', '').split()]
                word2vec.update({label: vector})

    with open('sample_sentences.txt', 'r+') as sample_sentences:
        for line in sample_sentences.readlines():
            words = tokenize(line)
            sample_words += words

    for word in sample_words:
        vector = word2vec[word] if word in word2vec else word2vec['<unknown>']
        vector_list.append(vector)

    return vector_list
