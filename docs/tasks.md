# List of Task that groups will be working on
The proposed task is to predict bitcoin price using social media information from twitter and reddit. We will split the task to 3 subtasks.
1. Predicting the Bitcoin price using historical bitcoin data or other publicly available datasets
2. Twitter sentiment analysis
3. Reddit sentiment analysis

Another option would be to include data from news pages and possibly other locations.

In the end we will use the sentiment analysis as a feature which will be added to the predictor from task 1

##### Basic structure

The basic structure for all tasks will be as follows.
* Determine a basic structure for the algorithm including what the algorithm will be predicting. For twitter eg. one could do hashtag prediction or emoji prediction as a proxy for sentiment prediction.
* Find a good dataset
* Write a dataparser
* Code the model
* Train
