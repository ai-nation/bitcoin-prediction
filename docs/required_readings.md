
Siraj Raval:

https://www.youtube.com/watch?v=G5Mx7yYdEhE&t=1113s


PyTorch Intro:


Good general overview (vid 1-4 = ML in general, from vid 4 end PyTorch)
https://www.youtube.com/playlist?list=PLlMkM4tgfjnJ3I-dbhO9JTw7gNty6o_2m&pbjreload=10

LSTM:

LSTM is a version of a NN which we will need throughout the weekend. You should read this beforehand:

https://colah.github.io/posts/2015-08-Understanding-LSTMs/
https://www.youtube.com/watch?v=WCUNPb-5EYI

