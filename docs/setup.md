# Setup

In order for this weekend to be a productive one, you are expected to come prepared and have the following software/tools installed on your computer and ready to go.

## Installation

* IDE like PyCharm (or ViM with equivalent plguins)
* Ptpython (when running pytorch with conda (required in windows) you have to locally install pip into your virtual environment and locally install ptpython if you had previous python distributions installed or another python interpreter is in your path)
* Anaconda3 + pytorch (recommended) or manually install Python 3.6x numpy, pandas, matplotlib, scikit-learn, pytorch 
* http://pytorch.org/ for pytorch instructions
* For Windows: conda install -c peterjc123 pytorch (in your virt. env) 
