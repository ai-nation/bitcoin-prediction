# Bitcoin Prediction

This is a ML hackathon project to predict bitcoin prices using PyTorch.

## Setup

```bash
cp .env.example .env # Put API keys in .env, not .env.example
```

## Author(s)

Emanuele Mazzotta  
Luca Naef  
Severin Bahman  
Franco de Bardeci  
Peter Unger  
Vincent Küttel  
David  

## License

[MIT License](https://emanuelemazzotta.com/mit-license/Emanuele%20Mazzotta,Luca%20Naef) © Emanuele Mazzotta, Luca Naef

